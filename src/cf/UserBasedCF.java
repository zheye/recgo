package cf;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import common.DataSet;
import common.Record;

public class UserBasedCF {
	private DataSet trainingDataSet = new DataSet();
//	private DataSet testDataSet = new DataSet();
	
	private void searchNeighborhoods(){
		//
	}
	
	
	//Sw-u
	private double calculatePCC(int userW, int userU){	//PCC: Pearson Correlation Coefficient
		HashMap<Integer, Double> uRatingSum = new HashMap<>();
		HashMap<Integer, HashSet<Integer>> uRatedItems = new HashMap<>();
		HashMap<Integer, Double> uRatingAvg = new HashMap<>(); 

		for(Record record: trainingDataSet.getRecordList()){
			//
			if(uRatingSum.containsKey(record.getUser())){
				uRatingSum.put(record.getUser(), record.getRating()+uRatingSum.get(record.getUser()));
			}
			else{
				uRatingSum.put(record.getUser(), record.getRating());
			}
			//
			if(uRatedItems.containsKey(record.getUser())){
				HashSet<Integer> uSet = uRatedItems.get(record.getUser());
				uSet.add(record.getItem());
				uRatedItems.put(record.getUser(), uSet);
			}
			else{
				HashSet<Integer> uSet = new HashSet<>();
				uSet.add(record.getItem());
				uRatedItems.put(record.getUser(), uSet);
			}
		}//for-each loop

		//
		for(Map.Entry<Integer, Double> entry: uRatingSum.entrySet()){
			uRatingAvg.put(entry.getKey(), uRatingSum.get(entry.getKey())/(double)(uRatedItems.get(entry.getKey()).size()));	//Do you understand!
		}// for-each loop
		
		double minusProductSum = 0;
		double userUMinusSquareSum = 0;
		double userWMinusSquareSum = 0;
		for(Record record: trainingDataSet.getRecordList()){
			if(record.getItem() != 0 ){	//isCoratedItem(record.getItem())
				 double userUMinus = trainingDataSet.obtainRatingByUserAndItem(userU, record.getItem())-uRatingAvg.get(userU);
				 double userWMinus = trainingDataSet.obtainRatingByUserAndItem(userW, record.getItem())-uRatingAvg.get(userW);
				 minusProductSum += userUMinus*userWMinus;
				 userUMinusSquareSum += Math.pow(userUMinus, 2);
				 userWMinusSquareSum += Math.pow(userWMinus, 2);
			}
		}
		
		double pcc = minusProductSum/(Math.sqrt(userUMinusSquareSum)*Math.sqrt(userWMinusSquareSum));
		
		return pcc;
	}


	private void process() {
		calculatePCC(0, 0);	//2333333333333.....
	}//process
	
	//start a user-based CF
	public void start(){
		trainingDataSet.makeDataSet("E:\\workspace\\Recgo\\ds\\u1.base");
		process();
	}

	
}
