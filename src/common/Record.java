package common;

public class Record {
	private int user;
	private int item;
	private double rating;	//using double is a better chose.
	
	//
	public double getRating() {
		return rating;
	}
	public void setRating(int rating) {
		this.rating = rating;
	}
	public int getUser() {
		return user;
	}
	public int getItem() {
		return item;
	}
	
	//
	public Record(int user, int item, int rating) {
		this.user = user;
		this.item = item;
		this.rating = rating;
	}
	
}
