package common;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import common.Record;

public class DataSet {
	//
	private ArrayList<Record> recordList = new ArrayList<>();
	
	
	//all of records
	public ArrayList<Record> getRecordList() {
		return recordList;
	}

	
	//add a record
	private void addRecord(Record record){
		recordList.add(record);
	}

	public void makeDataSet(String path){
		try{
			File trainingFile = new File(path);
			InputStreamReader isr = new InputStreamReader(new FileInputStream(trainingFile));
			BufferedReader br = new BufferedReader(isr);
			
			String line = "";	//a line is a record(for user's rating).
			while(line != null){	//java.lang.NullPointerException
				line = br.readLine();
				if(line != null){
					String[] record = line.split("\t");	// user, item, rating, timestamp
					
					this.addRecord(
							new Record(Integer.valueOf(record[0]), Integer.valueOf(record[1]), Integer.valueOf(record[2]))
					);	//" = new DataSet()" before use.
					
				}
			}
			
			br.close();
		}
		catch (Exception e){
			e.printStackTrace();
		}
		finally {
			//do nothing.
		}
	}

	public int getSize(){
		return recordList.size();
	}
	
	public double obtainRatingByUserAndItem(int user, int item){
		for(Record record: recordList){
			if(record.getUser()==user && record.getItem()==item){
				return record.getRating();
			}
		}
		return 0;	//should never be here.(BUG.......)
	}
	
	//load and generate a DataSet object using path
	public DataSet() {
		//
	}
	
	public DataSet(String path) {
		this.makeDataSet(path);
	}
	
}
