package af;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;

public class AverageFilling {
	private DataSet trainingDataSet = new DataSet();
	
	//load and make DataSet
	private void makeTrainingDataSet(){
//		Scanner in = new Scanner(System.in);
//		System.out.println("training dataset path:");
//		String trainingPath = in.nextLine();
//		in.close();
		
		try{
			File trainingFile = new File("E:\\workspace\\Recgo\\ds\\u1.base");
			InputStreamReader isr = new InputStreamReader(new FileInputStream(trainingFile));
			BufferedReader br = new BufferedReader(isr);
			
			String line = "";	//a line is a record(for user's rating).
			while(line != null){	//java.lang.NullPointerException
				line = br.readLine();
				if(line != null){
					String[] record = line.split("\t");	// user, item, rating, timestamp
					
					trainingDataSet.addRecord(
							new Record(Integer.valueOf(record[0]), Integer.valueOf(record[1]), Integer.valueOf(record[2]))
					);	//" = new DataSet()" before use.
					
				}
			}
			
			br.close();
		}
		catch (Exception e){
			e.printStackTrace();
		}
		finally {
			//do nothing.
		}
	}
	
	//start a AF
	public void start(){
		makeTrainingDataSet();
		trainingDataSet.analyze();
		Prediction prediction = new Prediction(trainingDataSet);
		prediction.predict();
	}
}
