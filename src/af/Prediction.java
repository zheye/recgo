package af;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;

public class Prediction {
	private DataSet trainingDataSet;
	private DataSet testDataSet = new DataSet();
	
	private double rate(int user, int item, int rule){
		double userAverage = trainingDataSet.uAverage.containsKey(user) ? trainingDataSet.uAverage.get(user) : trainingDataSet.globalAverage;
		double itemAverage = trainingDataSet.iAverage.containsKey(item) ? trainingDataSet.iAverage.get(item) : trainingDataSet.globalAverage;
		double userBias = trainingDataSet.uBias.containsKey(user) ? trainingDataSet.uBias.get(user) : 0;
		double itemBias = trainingDataSet.iBias.containsKey(item) ? trainingDataSet.iBias.get(item) : 0;

		switch(rule){
			case 1:
				return userAverage;
			case 2:	
				return itemAverage;
			case 3:
				return (userAverage + itemAverage) / 2;
			case 4:
				return userBias + itemAverage;
			case 5:
				return userAverage + itemBias;
			case 6:
				return trainingDataSet.globalAverage + userBias + itemBias;
			default:
				return userAverage;
		}
		
	}//rate

	private void makeTestDataSet(){
//		Scanner in = new Scanner(System.in);
//		System.out.println("test dataset path:");
//		String testPath = in.nextLine();
//		in.close();
		
		try{
			File testFile = new File("E:\\workspace\\Recgo\\ds\\u1.test");
			InputStreamReader isr = new InputStreamReader(new FileInputStream(testFile));
			BufferedReader br = new BufferedReader(isr);
			
			String line = "";	//a line is a record(for user's rating).
			while(line != null){	//java.lang.NullPointerException
				line = br.readLine();
				if(line != null){
					String[] record = line.split("\t");	// user, item, rating, timestamp
					
					testDataSet.addRecord(
							new Record(Integer.valueOf(record[0]), Integer.valueOf(record[1]), Integer.valueOf(record[2]))
					);	//" = new DataSet()" before use.
					
				}
			}
			
			br.close();
		}
		catch (Exception e){
			e.printStackTrace();
		}
		finally {
			//do nothing.
		}
	}
	
	public void predict(){
		makeTestDataSet();
		
//		Scanner in = new Scanner(System.in);
//		System.out.println("specify a directory to save result file:");
//		String resultDir = in.nextLine();
//		in.close();
		
		for(int i=1; i<=6; ++i){
			double MAESum = 0.0;
			double RMSESum =0.0;
			double ratedSum = 0.0;
			try{
				File resultFile = new File("E:\\workspace\\Recgo\\result"+"\\v"+i+".result");  
				resultFile.createNewFile();
				BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(resultFile));
				for(Record record: testDataSet.recordList){
					ratedSum++;
					double rateValue = rate(record.getUserCodeName(), record.getItemCodeName(), i);
					double errorValue = record.getRating() - rateValue;
					MAESum += Math.abs(errorValue);
					RMSESum += Math.pow(errorValue, 2);
					bufferedWriter.write(record.getUserCodeName()+"\t"+record.getItemCodeName()+"\t"+rateValue+"\r\n");
				}
				bufferedWriter.flush();
				bufferedWriter.close();
				
				evaluate(MAESum, RMSESum, ratedSum, i);
			}
			catch(Exception e){
				e.printStackTrace();
			}
			
		}
		
		System.out.println("work done. please view target directory.");
	}//predict
	
	private void evaluate(double MAESum, double RMSESum, double ratedSum, int rule){
		System.out.println("Prediction rule No."+rule);
		System.out.println("MAE: "+MAESum/ratedSum);
		System.out.println("RMSE: "+Math.sqrt(RMSESum/ratedSum));
		System.out.println("====================================================================");
	} 
	
	public Prediction(DataSet dataSet) {
		this.trainingDataSet = dataSet;
	}
	
}
