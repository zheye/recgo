package af;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class DataSet {
	//everything is double! not int or float
	public ArrayList<Record> recordList = new ArrayList<>();
	
	public double globalRatingSum;
	public double globalRatedNum;
	
	public Map<Integer, Double> uRatingSum = new HashMap<>();
	public Map<Integer, Double> uBiasSum = new HashMap<>();
	public Map<Integer, Double> uRatedNum = new HashMap<>();
	
	public Map<Integer, Double> iRatingSum = new HashMap<>();
	public Map<Integer, Double> iBiasSum = new HashMap<>();
	public Map<Integer, Double> iRatedNum = new HashMap<>();
	
	public double globalAverage;
	public Map<Integer, Double> uAverage = new HashMap<>();
	public Map<Integer, Double> iAverage = new HashMap<>();
	public Map<Integer, Double> uBias = new HashMap<>();
	public Map<Integer, Double> iBias = new HashMap<>();
	
	//add a record
	public void addRecord(Record record){
		recordList.add(record);
	}
	
	public int getSize(){
		return recordList.size();
	}

	public void analyze(){
		//R, Ru, Ri
		globalRatingSum = 0;
		globalRatedNum = 0;
		for(Record record: recordList){
			globalRatedNum++;
			globalRatingSum += record.getRating();
			//for User
			if(uRatingSum.containsKey(record.getUserCodeName())){
				uRatingSum.put(record.getUserCodeName(), record.getRating()+uRatingSum.get(record.getUserCodeName()));
			}
			else{
				uRatingSum.put(record.getUserCodeName(), (double) record.getRating());
			}
			if(uRatedNum.containsKey(record.getUserCodeName())){ 
				uRatedNum.put(record.getUserCodeName(), uRatedNum.get(record.getUserCodeName())+1);
			}
			else{
				uRatedNum.put(record.getUserCodeName(), 1.0);
			}
			//for Item
			if(iRatingSum.containsKey(record.getItemCodeName())){
				iRatingSum.put(record.getItemCodeName(), record.getRating()+iRatingSum.get(record.getItemCodeName()));
			}
			else{
				iRatingSum.put(record.getItemCodeName(), (double) record.getRating());
			}
			if(iRatedNum.containsKey(record.getItemCodeName())){
				iRatedNum.put(record.getItemCodeName(), iRatedNum.get(record.getItemCodeName())+1);
			}
			else{
				iRatedNum.put(record.getItemCodeName(), 1.0);
			}
		}
		globalAverage = (double) (globalRatingSum / globalRatedNum);	//R
		Set<Integer> uKeySet = uRatingSum.keySet();
		for(int user: uKeySet){
			uAverage.put(user, (double) (uRatingSum.get(user)/uRatedNum.get(user)));	//Ru
		}
		Set<Integer> iKeySet = iRatingSum.keySet();
		for(int item: iKeySet){
			iAverage.put(item, (double) (iRatingSum.get(item)/iRatedNum.get(item)));	//Ri
		}
		
		//Bu, Bi
		for(Record record: recordList){
			//for User
			if(uBiasSum.containsKey(record.getUserCodeName())){
				uBiasSum.put(record.getUserCodeName(), record.getRating()-iAverage.get(record.getItemCodeName())+uBiasSum.get(record.getUserCodeName()));
			}
			else{
				uBiasSum.put(record.getUserCodeName(), record.getRating()-iAverage.get(record.getItemCodeName()));
			}
			//for Item
			if(iBiasSum.containsKey(record.getItemCodeName())){
				iBiasSum.put(record.getItemCodeName(),  (record.getRating()-uAverage.get(record.getUserCodeName())+iBiasSum.get(record.getItemCodeName())));
			}
			else{
				iBiasSum.put(record.getItemCodeName(), record.getRating()-uAverage.get(record.getUserCodeName()));
			}
		}
		for(int user: uKeySet){
			uBias.put(user, uBiasSum.get(user)/uRatedNum.get(user));	//Bu
		}
		for(int item: iKeySet){
			iBias.put(item, iBiasSum.get(item)/iRatedNum.get(item));	//Bi
		}
		
	}//analyze
	
}
