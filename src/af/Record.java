package af;

public class Record {
	private int userCodeName;
	private int itemCodeName;
	private int rating;
	
	//
	public int getRating() {
		return rating;
	}
	public void setRating(int rating) {
		this.rating = rating;
	}
	public int getUserCodeName() {
		return userCodeName;
	}
	public int getItemCodeName() {
		return itemCodeName;
	}
	
	//
	public Record(int userCodeName, int itemCodeName, int rating) {
		this.userCodeName = userCodeName;
		this.itemCodeName = itemCodeName;
		this.rating = rating;
	}
	
}
